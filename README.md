# python-pancake-metamask

สคริปต์ภาษา python + selenium เพื่อเทรดเหรียญแบบ multi-thread ที่ pancake
โดยทำงานคู่กับ metamask 

![ตัวอย่างการทำงาน](vdo/pancake-multithread.mp4)


# download geckodriver 
- main URL : https://github.com/mozilla/geckodriver/releases
- windows installation : https://github.com/mozilla/geckodriver/releases/download/v0.30.0/geckodriver-v0.30.0-win64.zip

# download metamask extension
- main URL : https://addons.mozilla.org/en-US/android/addon/ether-metamask/versions/
- firefox : https://addons.mozilla.org/firefox/downloads/file/3881289/metamask-10.8.0-an+fx.xpi

# ติดตั้ง selenium
```
- pip install selenium
- pip install webdriver-manager
```

# อธิบาย
- ดับเบิ้ลคลิ๊ก run-pancake.bat เพื่อโปรแกรมเปิด firefox อยากได้กี่เธรด ก็ระบุตอนเริ่มโปรแกรม
โดยโปรแกรมจะโหลดและตั้งค่าพื้นฐานให้  รวมถึงตั้งค่าเหรียญที่จะเทรดเบื้องต้นให้ด้วย 
- ถ้าจะระบุรหัส หรือเหรียญอื่น ให้แก้ใน code
- บางครั้ง จะมีบางเธรด ทำงานไม่ปกติ (หลุดการทำงาน) ให้ตั้งค่าหน้าจอของ pancake ให้พร้อมเอง 
และรอจนกว่าจะอยู่ที่หน้าจอ Swap
```
** โปรดใช้ด้วยความระมัดระวัง โปรแกรมพัฒนาด้วยความรวดเร็ว เขียนขำๆ เกร๋ๆ  เป็นแบบ script ไม่ได้มี Unit test หรือ
ส่วนตรวจจับ bug ใดๆ ทั้งสิ้น
```
