# name: pancake.finance swap race
# start: 17 dec 2021
# versions: 0.1 beta
# last update: 17 dec 2021
# by: VRTK  ( ksemvis@gmail.com )
#
#
# download python
#   windows : https://www.python.org/ftp/python/3.10.1/python-3.10.1-amd64.exe
#   mac :
#   linux :
#
# download geckodriver 
#   URL : https://github.com/mozilla/geckodriver/releases
#   windows file : https://github.com/mozilla/geckodriver/releases/download/v0.30.0/geckodriver-v0.30.0-win64.zip
#
# download metamask extension
#   URL : https://addons.mozilla.org/en-US/android/addon/ether-metamask/versions/
#   firefox : https://addons.mozilla.org/firefox/downloads/file/3881289/metamask-10.8.0-an+fx.xpi
#
# :: python library setup
# pip install selenium
# pip install webdriver-manager
#
from selenium import webdriver
from selenium.webdriver.firefox.service import Service
from webdriver_manager.firefox import GeckoDriverManager
from selenium.webdriver.firefox.options import Options 
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.common.exceptions import NoSuchElementException
from datetime import datetime
from time import sleep
from threading import Thread
import os

# from selenium.webdriver.support.ui import WebDriverWait
# from selenium.webdriver.support import expected_conditions as EC

class FirefoxDriver(Thread):    
    # ##################################################################
    # global configuration
    # ##################################################################
    SECRET_RECOVERY_PHRASE = "process leisure giggle finger coffee original walk cruise crawl method duty trap"
    NEW_PASSWORD = r'}7]%/}X)J>=\x}uk}"2d*'
    
    def __init__(self, name):
        Thread.__init__(self)
        self.name = name        
        self.FIREFOX_PATH = os.path.abspath(os.getcwd()) + r'\geckodriver.exe'
        self.EXT_PATH = os.path.abspath(os.getcwd()) + r'\metamask-10.8.0-an+fx.xpi'
        self.SECRET_RECOVERY_PHRASE = FirefoxDriver.SECRET_RECOVERY_PHRASE
        self.NEW_PASSWORD = FirefoxDriver.NEW_PASSWORD
        self.SRC_TOKEN_NAME = r'BNB'
        self.TAR_TOKEN_NAME = r'BUNNY'
        self.TOTAL_MOUNT = r'0.000123'
        self.CURRENT_STEP = 0
        self.START_SWAP = False        
        if (len(self.SECRET_RECOVERY_PHRASE) <= 0):
            self.SECRET_RECOVERY_PHRASE = input("please input SECRET RECOVERY PHRASE: ")
            FirefoxDriver.SECRET_RECOVERY_PHRASE = self.SECRET_RECOVERY_PHRASE
        if (len(self.NEW_PASSWORD) <= 0):
            self.NEW_PASSWORD = input("please input NEW PASSWORD: ")
            FirefoxDriver.NEW_PASSWORD = self.NEW_PASSWORD
            
    def run(self):
        self.step_run()
        
    def step_run(self):
        self.step10()
        self.step20()
        self.step30()
        self.step40()
        self.step50()
        self.step60()
        self.step70()
        self.step80()
        self.step90()
        self.step91()
        self.step100()
        while( not self.START_SWAP ):
            sleep(0.5)
        #self.hello()
        self.step200()
        
    # ##################################################################
    # By_Type = By.XPATH , By.CSS_SELECTOR
    def find_element(self, By_Type, path, wait=0, max_count=3):
        cmdExit = False
        err_count = 0
        while ( not cmdExit and err_count < max_count ):
            try:
                if (wait > 0):
                    sleep(wait)
                self.driver.find_element(By_Type, path).click()
                cmdExit = True
            except NoSuchElementException:
                err_count += 1
        #
        if (err_count >= max_count):
            print("max error : Element '" + path + "' not found...")
            
    def XPATH_click(self, path, wait=0, max_count=3):
        self.find_element( By.XPATH, path, wait, max_count )
        
    def CSS_click(self, path, wait=0, max_count=3):
        self.find_element( By.CSS_SELECTOR, path, wait, max_count )
        
        
    def step10(self):
        # ##################################################################
        self.CURRENT_STEP = 10
        # ##################################################################
        print("firefox starting...")
        services = Service(self.FIREFOX_PATH)
        options = webdriver.FirefoxOptions()
        self.driver = webdriver.Firefox(service=services, options=options)
        self.driver.implicitly_wait(20)
        
    def step20(self):
        # ##################################################################
        self.CURRENT_STEP = 20
        # ##################################################################
        print("metamask loading...")
        self.driver.install_addon(self.EXT_PATH, temporary=True)
        sleep(3)
        
    def step30(self):
        # ##################################################################
        self.CURRENT_STEP = 30
        # ##################################################################
        print("initial wallet...")
        # เปลี่ยนไป tab ล่าสุด
        self.driver.switch_to.window( self.driver.window_handles[-1])
        # ใส่รหัสให้ Metamask
        self.XPATH_click(r'//button[text()="Get Started"]')
        self.XPATH_click(r'//button[text()="Import wallet"]')
        self.XPATH_click(r'//button[text()="No Thanks"]')
        
    def step40(self):
        # ##################################################################
        self.CURRENT_STEP = 40
        # ##################################################################
        inputs = self.driver.find_elements(By.XPATH, '//input')
        inputs[0].send_keys(self.SECRET_RECOVERY_PHRASE)
        inputs[1].send_keys(self.NEW_PASSWORD)
        inputs[2].send_keys(self.NEW_PASSWORD)
        self.CSS_click(r'.first-time-flow__terms')
        self.XPATH_click(r'//button[text()="Import"]')
        self.XPATH_click(r'//button[text()="All Done"]')
        
    def step50(self):
        # ##################################################################
        self.CURRENT_STEP = 50
        # ##################################################################
        # ย้ายเพจไปที่ pancake
        print("going to pancake...")
        self.driver.execute_script("window.open('');")
        self.driver.switch_to.window( self.driver.window_handles[-1] )
        self.driver.get('https://pancakeswap.finance/swap')
        
    def step60(self):
        # ##################################################################
        self.CURRENT_STEP = 60
        # ##################################################################
        # เชื่อมต่อ wallet
        print("create pancake to metamask bridge...")
        self.XPATH_click(r'//button[text()="Connect Wallet"]')
        self.XPATH_click(r'//div[text()="Metamask"]')
        
    def step70(self):
        # ##################################################################
        self.CURRENT_STEP = 70
        # ##################################################################
        # confirm การเชื่อมต่อ
        print("confirm wallet...")
        sleep(1)
        self.driver.switch_to.window( self.driver.window_handles[-1] )
        self.XPATH_click(r'//button[text()="Next"]')
        self.XPATH_click(r'//button[text()="Connect"]')
        
    def step80(self):
        # ##################################################################
        self.CURRENT_STEP = 80
        # ##################################################################
        # confirm network ที่ extension
        # ปัญหาคือ การทำงานที่เป็นคนละ tab บางครั้งอาจทำให้หลุดการ ควบคุม
        # ควรใช้ image processing
        print("confirm network...")
        sleep(3)
        self.driver.switch_to.window( self.driver.window_handles[-1] )
        self.XPATH_click(r'//button[text()="Approve"]', wait=1, max_count=1)
        self.XPATH_click(r'//button[text()="Switch network"]', wait=1, max_count=1)
        
    def step90(self):
        # ##################################################################
        self.CURRENT_STEP = 90
        # ##################################################################
        # change Source Token to "BNB/ect"
        print("change source token to '" + self.SRC_TOKEN_NAME + "'")
        self.driver.switch_to.window( self.driver.window_handles[2] )
        sleep(2)
        # source
        self.driver.find_elements(By.XPATH,'//*[@id="pair"]')[0].click()
        inputs = self.driver.find_element(By.XPATH, '//*[@id="token-search-input"]')
        inputs.send_keys(self.SRC_TOKEN_NAME)
        sleep(1)
        inputs.send_keys(Keys.ENTER)
        sleep(1)
        
    def step91(self):
        # ##################################################################
        self.CURRENT_STEP = 91
        # ##################################################################
        # change Target Token to "Bunny/LOA/ect"
        print("change target token to '" + self.TAR_TOKEN_NAME + "'")
        self.driver.find_elements(By.XPATH,'//*[@id="pair"]')[1].click()
        inputs = self.driver.find_element(By.XPATH, '//*[@id="token-search-input"]')
        inputs.send_keys(self.TAR_TOKEN_NAME)
        sleep(1)
        inputs.send_keys(Keys.ENTER)
        sleep(1)
        
    def step100(self):
        # ##################################################################
        self.CURRENT_STEP = 100
        # ##################################################################
        # เปลี่ยนค่าเตรียมเทรด
        print("change token amount to '" + self.TOTAL_MOUNT + "'")
        # driver.switch_to.window(driver.window_handles[-1])
        sleep(1)
        src_token = self.driver.find_element(By.XPATH, '/html/body/div[1]/div[1]/div[3]/div/div[1]/div[1]/div[2]/div/div/div[1]/div/div[2]/div[1]/div[1]/div[2]/div/div[1]/div/input')
        src_token.clear()
        src_token.send_keys(self.TOTAL_MOUNT)
        sleep(1)
        
    def step200(self):
        # ##################################################################
        self.CURRENT_STEP = 200
        # ##################################################################
        # start swap process
        print("swap starting...")
        self.XPATH_click(r'//button[text()="Swap"]')
        check_total_div = '/html/body/div[1]/div[1]/div[2]/div[2]/div/div[1]/div/div'
        current_price_type1 = '/html/body/div[1]/div[1]/div[2]/div[2]/div/div[1]/div/div[5]/div/b'
        current_price_type2 = '/html/body/div[1]/div[1]/div[2]/div[2]/div/div[1]/div/div[4]/div/b'
        # loop check the 'accept' button
        cmdExit = False
        err_count = 0
        while( not cmdExit and err_count < 5 ):
            try:
                if (len(self.driver.find_elements(By.XPATH, check_total_div)) > 4):
                    self.XPATH_click(r'//button[text()="Accept"]')
                    sleep(1)
                self.XPATH_click(r'//button[text()="Confirm Swap"]')
                cmdExit = True
            except NoSuchElementException:
                err_count += 1
                
        # ## how to get current price 
        #while(True):
        #    print("get current price...")
        #    price = "0.0"
        #    if (len(self.driver.find_elements(By.XPATH, check_total_div)) == 4):
        #        # ตอนไม่มี accept
        #        price = self.driver.find_element(By.XPATH, current_price_type2).text
        #    else:
        #        # ตอนมี accept
        #        price = self.driver.find_element(By.XPATH, current_price_type1).text
        #        
        #    now = datetime.now()
        #    print(now.strftime("%H:%M:%S") + " " + price)
        #    sleep(1)
        # ##
        
        # ex: close 'swap' dialog
        if ( err_count >= 5 ):
            self.XPATH_click(r'/html/body/div[1]/div[1]/div[2]/div[1]/button')
            print ("can't swap process terminate...")
        else:
            # ##################################################################
            self.CURRENT_STEP = 900
            # ##################################################################
            # confirm
            print("confirm swap..")
            sleep(1)
            self.driver.switch_to.window( self.driver.window_handles[-1] )
            self.XPATH_click(r'//button[text()="Confirm"]')
            sleep(1)
            self.driver.switch_to.window( self.driver.window_handles[2] )
            self.XPATH_click(r'//button[text()="Close"]', wait=2, max_count=2)
            self.START_SWAP = False
            
        print("job done...")
        
    def enable_swap(self):
        self.START_SWAP = True
            
    def hello(self):
        print("hello, pancake swap from " + self.name)
        sleep(3)
        print("hello#2, pancake swap from " + self.name)
        
# Main
driver_thread = []
max_thread = abs( int(input("how many Thread do you want (1-10 max): ")) )
if max_thread > 10:
    max_thread = 10
    
for i in range( max_thread ):
    driver_thread.append(FirefoxDriver( "Thread " + str(i) ))
    driver_thread[-1].start()

ready = "nook"
while( ready != 'ok' ):
    ready = input("type 'ok' when all thread ready: ")

for i in range( max_thread ):
    driver_thread[i].enable_swap()

